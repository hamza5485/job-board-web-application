__author__ = 'Steve Cassidy'
"""
Steve Cassidy was my teacher for ITEC649 - Web Technology
Since he provided the skeleton of this app to all students, he is mentioned as the author.
"""

from bottle import Bottle, template, static_file, redirect
from interface import *
from users import *

POST = 'POST'
GET = 'GET'

app = Bottle()


@app.route('/')
def index(db):
    """
    gets a list of positions and pass on to index template
    :param db: sqlite3.connection Object
    :return: call to Bottle Simple Template Engine interface to render index.html template
    """
    data = position_list(db)
    context = create_context(db, 'index', data)  # create variable to pass to template
    return template('index', context)


@app.route('/about')
def about(db):
    """
    routes to /about page on domain
    :return: call to Bottle Simple Template Engine interface to render about.html template
    """
    context = create_context(db, 'about')  # create variable to pass to template
    return template('about', context)


@app.route('/positions/<position_id>')
def detail(db, position_id):
    """
    gets position via position_id and pass on to detail template
    :param db: sqlite3.connection Object
    :param position_id: positions ID in database
    :return: call to Bottle Simple Template Engine interface to render detail.html template
    """
    data = position_get(db, position_id)
    context = create_context(db, None, data)  # create variable to pass to template
    return template('detail', context)


@app.route('/login', method=POST)
def login(db):
    """
    attempt user login with provided credentials
    If user credentials are correct, session is created and user is redirected to index
    else login failed page is returned
    :param db: sqlite3.connection Object
    :return: call to Bottle Simple Template Engine interface to render login.html template
    """
    nick = request.forms.get('nick')
    password = request.forms.get('password')
    if check_login(db, nick, password):
        generate_session(db, nick)
        response.set_header('Location', '/')  # add route in response header
        redirect('/', 302)  # set response code
    else:
        context = create_context(db, 'failed')  # create variable to pass to template
        return template('login', context)


@app.route('/logout', method=POST)
def attempt_logout(db):
    """
    log user out and delete user session
    :param db: sqlite3.connection Object
    :return: redirect to index
    """
    delete_session(db, response.get_header(COOKIE_NAME))
    redirect('/', 302)  # set response code


@app.route('/post', method=POST)
def add_post(db):
    """
    create new job with POST parameters
    if successful, redirect to index
    else error page is returned
    :param db: sqlite3.connection Object
    :return: call to Bottle Simple Template Engine interface to render error.html template
    """
    title = request.forms.get('title')
    location = request.forms.get('location')
    company = request.forms.get('company')
    descr = request.forms.get('description')
    nick = session_user(db)
    if position_add(db, nick, title, location, company, descr):  # check if position inserted in table
        redirect('/')
    else:
        context = create_context(db, None)
        return template('error', context)



@app.route('/static/<filename:path>')
def static(filename):
    """
    get static files by name and path and return
    :param filename: path + name of file stored in static root
    :return: helper function for serving static files
    """
    return static_file(filename=filename, root='static')


def create_context(db, active, data=None):
    """
    creates context variable to be passed on to template
    :param db: sqlite3.connection Object
    :param active: name of calling function, allows navbar to show current selected page. empty if page not on navbar
    :param data: any additional data required to serve on page, mostly holds results provided by db interface
    :return: dictionary
    """
    return {'title': 'JOBS', 'active': active, 'user': session_user(db), 'data': data}


if __name__ == '__main__':
    from bottle.ext import sqlite
    from database import *

    # install the database plugin
    app.install(sqlite.Plugin(dbfile=DATABASE_NAME))
    app.run(debug=True, port=8010)
