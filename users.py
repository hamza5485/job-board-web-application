"""
Created on Mar 26, 2012

@author: steve
"""

# this variable MUST be used as the name for the cookie used by this application
import uuid

from bottle import response, request

from database import password_hash

COOKIE_NAME = 'sessionid'


def check_login(db, usernick, password):
    """returns True if password matches stored"""
    cursor = db.cursor()
    sql = """SELECT * FROM users WHERE nick = ?"""
    cursor.execute(sql, (usernick,))
    record = cursor.fetchone()
    if record is not None:  # check if row exists in database
        if record[1] == password_hash(password):  # check if password matches
            return True
        else:
            return False
    else:
        return False


def generate_session(db, usernick):
    """create a new session and add a cookie to the response object (bottle.response)
    user must be a valid user in the database, if not, return None
    There should only be one session per user at any time, if there
    is already a session active, use the existing sessionid in the cookie
    """
    cursor = db.cursor()
    session_key = str(uuid.uuid4())  # password hash
    sql_user = """SELECT * FROM users WHERE nick = ?"""
    sql_session = """SELECT * FROM sessions WHERE usernick = ?"""
    sql_generate = """INSERT INTO sessions (sessionid, usernick) VALUES(?, ?)"""
    cursor.execute(sql_user, (usernick,))
    user_exists = cursor.fetchone()
    if user_exists is not None:  # check if user exists in table
        cursor.execute(sql_session, (usernick,))
        session_exists = cursor.fetchone()
        if session_exists is not None:  # check if session exists in table
            response.set_cookie(COOKIE_NAME, session_exists[0])  # create user session in HTTP Cookie
            return session_exists[0]
        else:
            cursor.execute(sql_generate, (session_key, usernick))
            response.set_cookie(COOKIE_NAME, session_key)  # create user session in HTTP Cookie
            return session_key
    else:
        return None


def delete_session(db, usernick):
    """remove all session table entries for this user"""
    cursor = db.cursor()
    sql = """DELETE FROM sessions WHERE usernick = ?"""
    cursor.execute(sql, (usernick,))
    response.delete_cookie(COOKIE_NAME)  # delete user session in HTTP Cookie


def session_user(db):
    """try to
    retrieve the user from the sessions table
    return usernick or None if no valid session is present"""
    cursor = db.cursor()
    sql = """SELECT * FROM sessions WHERE sessionid = ?"""
    cursor.execute(sql, (request.get_cookie(COOKIE_NAME),))
    record = cursor.fetchone()
    if record is not None:  # check if session exists in table
        return record[1]
    else:
        return None


