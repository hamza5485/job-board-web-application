"""
Database Model interface for the COMP249 Web Application assignment

@author: steve cassidy
"""


def position_list(db, limit=10):
    """Return a list of positions ordered by date
    db is a database connection
    return at most limit positions (default 10)

    Returns a list of tuples  (id, timestamp, owner, title, location, company, description)
    """
    cursor = db.cursor()
    sql = """SELECT * FROM positions ORDER BY `timestamp` DESC LIMIT ?"""
    cursor.execute(sql, (limit,))
    records = cursor.fetchall()
    return records


def position_get(db, id):
    """Return the details of the position with the given id
    or None if there is no position with this id

    Returns a tuple (id, timestamp, owner, title, location, company, description)

    """
    cursor = db.cursor()
    sql = """SELECT * FROM positions WHERE id = ?"""
    cursor.execute(sql, (id,))
    record = cursor.fetchone()
    return record


def position_add(db, usernick, title, location, company, description):
    """Add a new post to the database.
    The date of the post will be the current time and date.
    Only add the record if usernick matches an existing user

    Return True if the record was added, False if not."""
    cursor = db.cursor()
    sql = """
INSERT INTO positions (title, location, company, description, owner)
SELECT ?, ?, ?, ?, users.nick FROM users WHERE users.nick = ?
"""
    cursor.execute(sql, (title, location, company, description, usernick))
    # cursor.rowcount: - retrieves number of rows affected by cursor.execute(); None if no rows affected.
    if cursor.rowcount:
        return True
    else:
        return False
